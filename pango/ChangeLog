commit bd2fd257f9f6f7ce095caa6c5c0e4332ef3cba75
Author: Matthias Clasen <mclasen@redhat.com>
Date:   Tue Feb 13 14:38:22 2018 -0500

    1.41.1

 NEWS         | 7 +++++++
 configure.ac | 2 +-
 meson.build  | 2 +-
 3 files changed, 9 insertions(+), 2 deletions(-)

commit 0f75002573abfe6494efec5973d07d23167da579
Author: Michael Catanzaro <mcatanzaro@igalia.com>
Date:   Mon Feb 12 12:19:41 2018 -0600

    Disable -Werror=undef

 meson.build | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

commit f553ec30390caddbbfb44336a48a51b63ba9e9f1
Author: Emmanuele Bassi <ebassi@gnome.org>
Date:   Mon Feb 12 12:23:42 2018 +0000

    meson: Update rules for introspection

    Building introspection should not be gated on cross-compilation: it's
    perfectly acceptable to use an helper binary to run the introspection
    scanner when cross-compiling — in fact, it's what projects like
    Yocto
    do. Instead, we should have an option to disable the introspection
    generation explicitly.

    Additionally, when building introspection data for ancillary Pango
    libraries, like PangoCairo or PangoXft, we should depend on the GIR
    target, instead of adding an `--include-uninstalled` extra argument
    for
    the introspection scanner; this allows building Pango as a sub-project
    of another project, and lets Meson deal with the appropriate paths and
    arguments when invoking the scanner.

 meson.build       |   2 +-
 meson_options.txt |   4 +
 pango/meson.build | 314
 +++++++++++++++++++++++++++++-------------------------
 3 files changed, 176 insertions(+), 144 deletions(-)

commit b0a75495df58d71d8809c223b6a327f1bc90c844
Author: Khaled Hosny <khaledhosny@eglug.org>
Date:   Tue Nov 14 15:55:14 2017 +0200

    Use the new FriBiDi API when available

 pango/pango-bidi-type.c | 22 ++++++++++++++++++++++
 1 file changed, 22 insertions(+)

commit 526c9b586a32ebee2e269ecd41652b215cd21274
Author: Khaled Hosny <khaledhosny@eglug.org>
Date:   Tue Nov 14 15:12:23 2017 +0200

    Avoid converting to UTF-32 for FriBiDi

    Can’t tell if this is any faster, but does not look like a complex
    thing to do, so why not.

 pango/pango-bidi-type.c | 64
 ++++++++++++++++++++++++-------------------------
 1 file changed, 31 insertions(+), 33 deletions(-)

commit 0a71013dfc3a5199f0632bb99410523949bf49d7
Author: Khaled Hosny <khaledhosny@eglug.org>
Date:   Mon Nov 13 15:27:37 2017 +0200

    Drop now unused mini-fribidi

 Makefile.am                                  |    1 -
 configure.ac                                 |    1 -
 docs/Makefile.am                             |    1 -
 docs/meson.build                             |    1 -
 pango/mini-fribidi/Makefile.am               |   32 -
 pango/mini-fribidi/README                    |   23 -
 pango/mini-fribidi/fribidi.c                 |  967 -----
 pango/mini-fribidi/fribidi.h                 |   56 -
 pango/mini-fribidi/fribidi.patch             | 1369 ------
 pango/mini-fribidi/fribidi_char_type.c       |   75 -
 pango/mini-fribidi/fribidi_config.h          |   33 -
 pango/mini-fribidi/fribidi_tab_char_type_2.i | 5884
 --------------------------
 pango/mini-fribidi/fribidi_types.c           |   86 -
 pango/mini-fribidi/fribidi_types.h           |  313 --
 pango/mini-fribidi/fribidi_types.i           |   21 -
 pango/mini-fribidi/meson.build               |   17 -
 16 files changed, 8880 deletions(-)

commit 5e8cb02bec01e4a9b0ddff173889043d9db65fbc
Author: Khaled Hosny <khaledhosny@eglug.org>
Date:   Mon Nov 13 15:25:35 2017 +0200

    Switch to using external FriBiDi

 configure.ac            |  4 ++++
 meson.build             |  4 ++++
 pango/Makefile.am       | 14 ++++---------
 pango/meson.build       |  4 +---
 pango/pango-bidi-type.c | 54
 +++++++++++++++++++++++++++++++++++--------------
 5 files changed, 52 insertions(+), 28 deletions(-)

commit 1addea2febecb9e3511329b985d5566de6d8369e
Author: Philip Withnall <withnall@endlessm.com>
Date:   Tue Jan 9 11:05:29 2018 +0000

    build: Only enable freetype if fontconfig is also available

    This changes meson.build to match what configure.ac already does. The
    code doesn’t have separate conditions for whether fontconfig and
    freetype are available: if freetype compilation is enabled, it assumes
    that fontconfig is also available.

    Previously, systems with freetype available, but no fontconfig, would
    fail to compile Pango due to trying to link against non-existent
    fontconfig symbols.

    Signed-off-by: Philip Withnall <withnall@endlessm.com>

    https://bugzilla.gnome.org/show_bug.cgi?id=792363

 meson.build | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

commit 4120b76eed6daeec38d844ecf819977899f4edd7
Author: Matthias Clasen <mclasen@redhat.com>
Date:   Mon Jan 8 22:01:17 2018 -0500

    1.41.0

 NEWS | 6 ++++++
 1 file changed, 6 insertions(+)

commit 3ccf578a2d39350d19af12bceaf700cb6b178436
Author: Christian Hergert <chergert@redhat.com>
Date:   Thu Jan 4 23:11:44 2018 -0800

    fontmap: fix warnings from recent g_object_ref() changes

    https://bugzilla.gnome.org/show_bug.cgi?id=792231

 pango/pangofc-fontmap.c | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)
