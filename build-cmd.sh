#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

PROJECT=pango
LICENSE=README
VERSION="1.41.1"
SOURCE_DIR="$PROJECT"


if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"
GLIB_INCLUDE="${stage}"/packages/include/glib
FREETYPE_INCLUDE="${stage}"/packages/include/freetype2/freetype
FONTCONFIG_INCLUDE="${stage}"/packages/include/fontconfig
CAIRO_INCLUDE="${stage}"/packages/include/cairo
HARFBUZZ_INCLUDE="${stage}"/packages/include/harfbuzz
 
[ -f "$FONTCONFIG_INCLUDE"/fcfreetype.h ] || fail "You haven't installed the fontconfig package yet."
[ -f "$CAIRO_INCLUDE"/cairo.h ] || fail "You haven't installed the cairo package yet."
[ -f "$FREETYPE_INCLUDE"/freetype.h ] || fail "You haven't installed the freetype package yet."
[ -f "$GLIB_INCLUDE"/glib.h ] || fail "You haven't installed the glib package yet."
[ -f "$HARFBUZZ_INCLUDE"/hb.h ] || fail "You haven't installed the harfbuzz package yet."
echo export PKG_CONFIG_PATH="$stage/packages/release/lib/pkgconfig/"
echo export PKG_CONFIG_LIBDIR="$stage/packages/release/lib/"

#CAIRO_SOURCE_DIR="$PROJECT"

# load autbuild provided shell functions and variables
source_environment_tempfile="${stage}/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

build=${AUTOBUILD_BUILD_ID:=0}
echo "${VERSION}.${build}" > "${stage}/VERSION.txt"

case "$AUTOBUILD_PLATFORM" in
    "linux")

       # Prefer gcc-4.9 if available.
        if [[ -x /usr/bin/gcc-4.9 && -x /usr/bin/g++-4.9 ]]; then
            export CC=/usr/bin/gcc-4.9
            export CXX=/usr/bin/g++-4.9
        fi
        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
#-I$stage/packages/include-I$GLIB_INCLUDE -I$CAIRO_INCLUDE -I$HARFBUZZ_INCLUDE -I$FONTCONFIG_INCLUDE -I$FREETYPE_INCLUDE
        pushd "$TOP/$SOURCE_DIR"
           export PKG_CONFIG_PATH="$stage/packages/lib/pkgconfig"
           export PATH="$stage/packages/include":$PATH
           CFLAGS="-I$GLIB_INCLUDE -I$CAIRO_INCLUDE -I$HARFBUZZ_INCLUDE -I$FREETYPE_INCLUDE -I$FONTCONFIG_INCLUDE $opts -O3 -fPIC -DPIC"
           LDFLAGS="-L$stage/packages/lib/release" 
           CC="$CC -m32" ./configure --prefix="$stage"
            make
            make install
		    mkdir -p "$stage/include/pango"
		    cp -a pango/*.h "$stage/include/pango/"
        popd

        mv lib release
        mkdir -p lib
        mv release lib


    ;;
    "linux64")

       # Prefer gcc-6 if available.
        if [[ -x /usr/bin/gcc-6 && -x /usr/bin/g++-6 ]]; then
            export CC=/usr/bin/gcc-6
            export CXX=/usr/bin/g++-6
        fi
        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
#-I$stage/packages/include-I$GLIB_INCLUDE -I$CAIRO_INCLUDE -I$HARFBUZZ_INCLUDE -I$FONTCONFIG_INCLUDE -I$FREETYPE_INCLUDE
        export CPATH="$stage/packages/include/glib:$stage/packages/include/harfbuzz"
        pushd "$TOP/$SOURCE_DIR"
            export AM_CONDITIONAL="$AM_CONDITIONAL:$ENABLE_GTK_DOC"
            
            export GTKDOC_DEPS_CFLAGS="$stage/packages/share/gtk-doc/"
            export GTKDOC_DEPS_LIBS="$stage/packages/lib/release"
            export FREETYPE_CFLAGS="$stage/packages/include/freetype/" 
	        export FREETYPE_LIBS="$stage/packages/lib/release/libfreetype.so -lfreetype"
	        export HARFBUZZ_CFLAGS="$stage/packages/include/harfbuzz/"
	        export HARFBUZZ_LIBS="$stage/packages/lib/release/"
	        export CAIRO_CFLAGS="$stage/packages/include/cairo/"
	        export CAIRO_LIBS="$stage/packages/lib/release/libcairo.so -lcairo"
	        export FONTCONFIG_CFLAGS="$stage/packages/include/fontconfig/"
	        export FONTCONFIG_LIBS="$stage/packages/lib/release/libfontconfig.so"	      	          
	        export GLIB_CFLAGS="$stage/packages/include/glib/"
	        export GLIB_LIBS="$stage/packages/lib/release/libglib-2.0.so -lglib-2.0"	        
           export PATH="$stage/packages/include":$PATH
           CFLAGS="-I$stage/packages/include \
           -I$GLIB_INCLUDE \
           -I$CAIRO_INCLUDE \
           -I$HARFBUZZ_INCLUDE \
           -I$FREETYPE_INCLUDE \
           -I$FONTCONFIG_INCLUDE $opts"
           LDFLAGS="-L$stage/packages/lib/release"
           CC="$CC -m64" ./configure --prefix="$stage" --includedir="$stage/packages/include/glib/ $stage/packages/include/harfbuzz/"
            make
            make install
		    mkdir -p "$stage/include/pango"
		    cp -a pango/*.h "$stage/include/pango/"
        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        echo "platform not supported"
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"



